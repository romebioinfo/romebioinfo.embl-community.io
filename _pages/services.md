---
title: "EMBL Rome Bioinformatics Services - Services"
layout: gridlay
excerpt: "EMBL Rome Bioinformatics Services -- Services."
sitemap: false
permalink: /services/
---


# Services



{% assign number_printed = 0 %}
{% for service in site.data.services %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if service.highlight == 1 %}

{% if even_odd == 0 %}
<div class="row">
{% endif %}

<div class="col-sm-6 clearfix">
 <div class="well">
  <pubtit>{{ service.title }}</pubtit>
  <img src="{{ site.url }}{{ site.baseurl }}/images/servicepic/{{ service.image }}" class="img-responsive" width="33%" style="float: left" />
  <p>{{ service.description }}</p>
 </div>
</div>

{% assign number_printed = number_printed | plus: 1 %}

{% if even_odd == 1 %}
</div>
{% endif %}

{% endif %}
{% endfor %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if even_odd == 1 %}
</div>
{% endif %}

<img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/advice_0Z3A7005-resize.png" alt="Slide 6" />

<p> &nbsp; </p>

<br> These services are developped in collaboration with GBCS in Heidelberg. Visit their [website](https://gbcs.embl.de/portal/tiki-index.php){:target="_blank"}.<br>

# Details

## ChIP-Seq

The processing of single- and paired-end data is currently provided through a galaxy workflow as requested by biologists.

You receive the following from us:

  * QC (quality control)<br>
    + **A multiqc report** containing different metrics: [single-end](../html_examples/general_multiqc-singleend_chipseq.html) and [paired-end](../html_examples/general_multiqc-pairedend_chipseq.html).<br>
    + **pca and samples correlation plots** <br>
    + **Fingerprint and coverage plots** <br><br>

  * Signal <br>
    + **Bam files** <br>
    + **bigwig files** <br>
    + **MACS2 peaks** <br><br>

  * Analysis <br>
    + **gene_ontologies**
    + **Motifs detection**

#### Further analysis

Custom plots and analysis can be provided **within the framework of a collaboration**. This includes but is not limited to: <br>

   * Heatmaps and clustering: K-mean, hierarchical, custom feature based, supervised clustering.
   * Advanced motif analysis with ensembl methods.
   * Gene ontologies with ChIPEnrich, clusterProfiler.
   * Graphical analysis: Boxplots, violin plots, barplots, piecharts, etc.
   * Differential binding analysis.
   * Hidden markov modelling: Typically used to describe chromatin states.
   * Linear regression analysis.
   * Sub-group definition by venn diagram analysis.
   * Refined peak calling with hiddenDomains, SICER, SPP, etc.
   * Tissue specificity analysis.


## RNA-Seq

The processing of single- and paired-end data is currently provided through a galaxy workflow as requested by biologists.

You receive the following from us:

  * QC (quality control)<br>
    + **A multiqc report** containing different metrics: [single-end](../html_examples/general_multiqc-singleend_rnaseq.html) and [paired-end](../html_examples/general_multiqc-pairedend_rnaseq.html).<br>
    + **Counts table**: Raw and TPM Normalized counts from Salmon (transcriptome based) and Featurecounts (reference genome based)
    + **Correlation and PCA**: Using no, CPM, RPKM and TPM normalization.

  * Signal <br>
    + **Bam files** <br>
    + **bigwig files**: Using no, CPM, RPKM and TPM normalization. <br>

#### Further analysis

Custom plots and analysis can be provided **within the framework of a collaboration**. This includes but is not limited to: <br>

   * Differential expression analysis with DEseq2 and/or EdgeR
   * Heatmaps and clustering of differentially expressed genes.
   * Gene ontologies of differentially expressed genes.
   * Graphical analysis: Boxplots, violin plots, barplots, piecharts, etc.


## ATAC-Seq

In construction