---
title: "EMBL Rome Bioinformatics Services - Resources"
layout: gridlay
excerpt: "EMBL Rome Bioinformatics Services - Resources"
sitemap: false
permalink: /resources/
---


# Table of Contents

1. [ChIP-Seq](#chipseq)
2. [Single-cell ChIP-Seq](#scchipseq)
3. [RNA-Seq](#rnaseq)
4. [Single-cell RNA-Seq](#scrnaseq)
5. [ATAC-Seq](#atacseq)
6. [Snakemake](#snakemake)

<br>
<br>

#### ChIP-Seq

  + CutnRun orginal article: [An efficient targeted nuclease strategy for high-resolution mapping of DNA binding sites](https://elifesciences.org/articles/21856)

<br>
<br>

#### Single-cell ChIP-Seq

  + CutnTag by Henikoff lab: [CUT&Tag for efficient epigenomic profiling of small samples and single cells](https://www.nature.com/articles/s41467-019-09982-5)
  + Low throughput CutnRun by Fazzio lab: [Profiling of Pluripotency Factors in Single Cells and Early Embryos](https://www.sciencedirect.com/science/article/pii/S0092867419302764?via%3Dihub)
  + CoBATCH by He lab: [CoBATCH for High-Throughput Single-Cell Epigenomic Profiling](https://www.cell.com/molecular-cell/fulltext/S1097-2765(19)30545-3)

<br>
<br>

#### RNA-Seq

  + Oasis2 for small rna detection and DE by Bonn lab: [Oasis 2: improved online analysis of small RNA-seq data](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2047-z)
  + Kallisto, Salmon, etc by Tom Smith: [Why you should use alignment-independent quantification for RNA-Seq](https://cgatoxford.wordpress.com/2016/08/17/why-you-should-stop-using-featurecounts-htseq-or-cufflinks2-and-start-using-kallisto-salmon-or-sailfish/)


<br>
<br>

#### Single-cell RNA-Seq

  + sc-RNA-seq tutorial	by Hemberg lab:	[Analysis of single cell RNA-seq data](https://scrnaseq-course.cog.sanger.ac.uk/website/index.html)

<br>
<br>

#### ATAC-Seq

  + NucleoATAC by Greenleaf lab: [Structured nucleosome fingerprints enable high-resolution mapping of chromatin architecture within regulatory regions](https://genome.cshlp.org/content/25/11/1757)

<br>
<br>

#### Snakemake

  + Introduction tutorial by Titus Brown: [Intro to workflows for efficient automated data analysis, using snakemake](https://hackmd.io/7k6JKE07Q4aCgyNmKQJ8Iw?view)