---
title: "EMBL Rome Bioinformatics Services - Training"
layout: gridlay
excerpt: "EMBL Rome Bioinformatics Services - Training"
sitemap: false
permalink: /training/
---


# Training

<img src="{{ site.url }}{{ site.baseurl }}/images/slider7001400/20190617_RomeBI_1400x700px4.jpg" alt="Slide 4" height="350" width="700"/>

**These trainings are for the moment only opened to EMBL employees**. They are provided in collaboration with [Bio-IT](https://bio-it.embl.de/). Please email nicolas.descostes@embl.it to register.
<br/>
<br/>


| Module  |  Requirements &nbsp;&nbsp; | Date  |  Teacher | Link |
|----------------------------------------------------------|--------------------------------------------------------|----------------------|-------------------|-------------------------------------------------|
||||||




<br/>
<br/>

## Previous workshops

   + October 2018: Intro to programming with R
   + March 2019: Intro to Unix command line. [Material](https://git.embl.de/grp-bio-it/linuxcommandline)
   + April 2019: Intro to HPC usage. [Material](https://git.embl.de/grp-bio-it/embl_hpc)
   + May 2019: Intro to Python (by Gregor Moenke)
   + June 2019: Intro to programming with R part 1. [Material](https://descostesn.github.io/RIntroProgBio/)
   + July 2019: Intro to programming with R part2. [Material](https://descostesn.github.io/RIntroProgBio/)
   + October 2019: ChIP-Seq with Galaxy. [Material](https://git.embl.de/descoste/galaxychipseqworkshop)
   + November 2019: RNA-Seq with Galaxy. [Material](https://git.embl.de/descoste/galaxyrnaseqworkshop)
   + December 2019:  Intro to git. [Material](https://git.embl.de/descoste/introduction-git-bioit-workshop)
   + May 2020: R and visualization. [Material](https://grp-bio-it-workshops.embl-community.io/introduction-to-R/)
   + February 2021:  R and visualization. [Material](https://git.embl.de/grp-bio-it-workshops/introduction-to-R)
   + March 2021: Galaxy training. [Material](https://pad.bio-it.embl.de/gaIKCHSGToCFpUONaWSBvQ)

{% assign number_printed = 0 %}
{% for publi in site.data.publist %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if publi.highlight == 1 %}

{% if even_odd == 0 %}
<div class="row">
{% endif %}

<div class="col-sm-6 clearfix">
 <div class="well">
  <pubtit>{{ publi.title }}</pubtit>
  <img src="{{ site.url }}{{ site.baseurl }}/images/pubpic/{{ publi.image }}" class="img-responsive" width="33%" style="float: left" />
  <p>{{ publi.description }}</p>
  <p><em>{{ publi.authors }}</em></p>
  <p><strong><a href="{{ publi.link.url }}">{{ publi.link.display }}</a></strong></p>
  <p class="text-danger"><strong> {{ publi.news1 }}</strong></p>
  <p> {{ publi.news2 }}</p>
 </div>
</div>

{% assign number_printed = number_printed | plus: 1 %}

{% if even_odd == 1 %}
</div>
{% endif %}

{% endif %}
{% endfor %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if even_odd == 1 %}
</div>
{% endif %}

<p> &nbsp; </p>




